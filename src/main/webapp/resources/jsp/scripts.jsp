<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" /> "></script>
<script type="text/javascript" src="<c:url value="/resources/js/app.js" /> "></script>